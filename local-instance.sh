#!/bin/bash
# cluster start script for doing literally everything
# This script sets up a complete kubernetes installation for Rosenzweig.

# parsing incoming parameter, see: https://stackoverflow.com/a/14203146
POSITIONAL_ARGS=()
while [[ $# -gt 0 ]]; do
  case $1 in
    -p|--port-forward)
      PORT="$2"
      shift # past argument
      shift # past value
      ;;
    --debug)
      DEBUG=true
      echo "[DEBUG] enabled"
      shift # past argument
      ;;
    --slow)
      TIMEOUT="420s"
      shift
      ;;
    --help)
      printf ' 🚀 starting your cluster!\nparameters:\n  -p --port-forward [NUM] if set, a kubectl port-forward to existdb-0 will be started at the end.\n  --slow additional time for all timeouts\n  -w --build-website build an image for the website.\n  --debug increase verbosity to the max\n'
      exit 1
      ;;
    -w|--build-website)
      BUILD_WEBSITE=true
      shift # past argument
      ;;
    -*|--*)
      printf " 🧨\tUnknown option $1\n\trun with --help for more usage instructions"
      exit 1
      ;;
    *)
      POSITIONAL_ARGS+=("$1") # save positional arg
      shift # past argument
      ;;
  esac
done

# end script on failing command (exit code)
set -e

_PWD=$(pwd)
epoch=$(date +%s)
NAME="stardev"

if [ "${DEBUG}" == "true" ]; then
  printf "[DEBUG] NAME=${NAME}\n"
  printf "[DEBUG] epoch=${epoch}\n"
  DOCKER_OPT="--progress plain"
  DOCKER_OUT="/dev/stdout"
  HELM_OPT="--debug"
  HELM_OUT="/dev/stdout"
  K3D_OPT="--verbose"
  K3D_OUT="/dev/stdout"
  KUBECTL_OUT="/dev/stdout"
else
  DOCKER_OPT="--quiet"
  DOCKER_OUT="/dev/null"
  HELM_OUT="/dev/null"
  K3D_OPT=" "
  K3D_OUT="/dev/null"
  KUBECTL_OUT="/dev/null"
fi

[ -z "${TIMEOUT}" ] && TIMEOUT="200s"

# checking for dependencies
which k3d > /dev/null
which kubectl > /dev/null
which jq > /dev/null

if ! grep       "${NAME}.local" /etc/hosts ; then echo -n -e "🚨 ${NAME}.local not found in /etc/hosts.\n🚨 printf '127.0.0.1 ${NAME}.local' | sudo tee -a /etc/hosts\n" && exit 1; fi
if ! grep "rdfdb.${NAME}.local" /etc/hosts ; then echo -n -e "🚨 ${NAME}.local not found in /etc/hosts.\n🚨 printf '127.0.0.1 rdfdb.${NAME}.local' | sudo tee -a /etc/hosts\n" && exit 1; fi

# remove olde cluster
k3d cluster delete ${NAME}
k3d cluster create ${NAME} --k3s-arg "--disable=traefik@server:0" -p "80:80@loadbalancer"

docker pull harbor.gwdg.de/sub/star/rogerzweig:main-5a925bc6-local 
docker tag harbor.gwdg.de/sub/star/rogerzweig:main-5a925bc6-local harbor.gwdg.de/sub/star/rogerzweig:local
k3d image import --cluster ${NAME} \
    registry.k8s.io/ingress-nginx/controller:v1.10.0 \
    docker.io/eclipse/rdf4j-workbench:4.3.11 \
    harbor.gwdg.de/sub/star/rogerzweig:local

# add nginx-ingress controller (chart version: 4.10.0 → nginx version 1.10.0)
helm upgrade --install ingress-nginx ingress-nginx \
  --repo https://kubernetes.github.io/ingress-nginx \
  --namespace kube-system \
  --set controller.enableSnippets="true"\
  ${HELM_OPT} 1> ${HELM_OUT}

# wait for nginx service to become available
[ "${DEBUG}" == "true" ] && printf " 🐸\twaiting for ingress-nginx to become available\n"
kubectl --namespace kube-system wait deployments.apps ingress-nginx-controller --for=jsonpath='{.status.readyReplicas}'=1 --timeout=${TIMEOUT} 1> ${KUBECTL_OUT}
[ "${DEBUG}" == "true" ] && printf " 🐸\tingress-nginx is available now\n"

kubectl label ingressclasses.networking.k8s.io nginx ingressclass.kubernetes.io/is-default-class="true" 1> ${KUBECTL_OUT}

helm install ${NAME} star/ \
  --namespace ${NAME} \
  --create-namespace \
  --render-subchart-notes \
  --set "images.webserver.tag=local" \
  --set "images.webserver.repository=harbor.gwdg.de/sub/star/rogerzweig"


kubectl config set-context --current --namespace="${NAME}"

kubectl wait --for=condition=Ready pods --all --timeout=${TIMEOUT} 1> ${KUBECTL_OUT}